fn main() {
    let min = [1, 9, 3, 6, 5, 1];
    let max = [6, 4, 9, 7, 2, 9];

    let mut matches = 0;
    let mut i = min.clone();
    loop {
        // Check for a adjacents
        if i[0] == i[1] ||
            i[1] == i[2] ||
            i[2] == i[3] ||
            i[3] == i[4] ||
            i[4] == i[5]
        {
            // Check for incrementing
            if i[0] <= i[1] &&
                i[1] <= i[2] &&
                i[2] <= i[3] &&
                i[3] <= i[4] &&
                i[4] <= i[5]
            {
                //println!("{:?} matches", i);
                matches += 1;
            }
        }
        let mut x = 5;
        loop {
            i[x] += 1;
            if i[x] >= 10 {
                i[x] -= 10;
                x -= 1;
                continue
            }
            break;
        }
        if i[0] >= max[0] &&
            i[1] >= max[1] &&
            i[2] >= max[2] &&
            i[3] >= max[3] &&
            i[4] >= max[4] &&
            i[5] >= max[5] {
            break;
        }
    }
    println!("[Part 1] There are {} potential matches", matches);

    i = min.clone();
    matches = 0;
    loop {
        // Check for incrementing
        if i[0] <= i[1] &&
            i[1] <= i[2] &&
            i[2] <= i[3] &&
            i[3] <= i[4] &&
            i[4] <= i[5]
        {
            // Check for groups
            let mut idx = 0;
            let mut has_two_chain = false;
            let mut chain_size = 1;
            while idx < 5 {
                if i[idx] == i[idx+1] {
                    chain_size += 1;
                    idx += 1;
                }
                else {
                    if chain_size == 2 {
                        has_two_chain = true;
                    }
                    chain_size = 1;
                    idx += 1;
                }
            }
            if chain_size == 2 {
                has_two_chain = true;
            }
            if has_two_chain {
                matches += 1;
            }
        }
        let mut x = 5;
        loop {
            i[x] += 1;
            if i[x] >= 10 {
                i[x] -= 10;
                x -= 1;
                continue
            }
            break;
        }
        if i[0] >= max[0] &&
            i[1] >= max[1] &&
            i[2] >= max[2] &&
            i[3] >= max[3] &&
            i[4] >= max[4] &&
            i[5] >= max[5] {
            break;
        }
    }
    println!("[Part 2] There are {} potential matches", matches);
}
