use std::collections::VecDeque;
use std::collections::HashMap;

#[derive(Debug)]
#[derive(PartialEq)]
#[derive(Copy, Clone)]
pub enum Status {
    Running,
    Finished,
    WaitingForInput,
}

pub struct Result {
    pub status: Status,
    pub instruction: usize,
}

pub struct Computer {
    memory: HashMap<usize, i64>,
    position: usize,
    relative_base: i64,
    input: VecDeque<i64>,
    pub output: Vec<i64>,
}

#[derive(Debug)]
#[derive(Copy, Clone)]
enum Operation {
    Add,
    Mul,
    Input,
    Output,
    JumpIfTrue,
    JumpIfFalse,
    LessThan,
    Equals,
    AdjustRelativeBase,
    End, // 99
}

#[derive(Debug)]
#[derive(Copy, Clone)]
enum ParameterMode {
    Position,  // 0
    Immediate, // 1
    Relative,  // 2
}

impl ParameterMode {
    pub fn from_int(i: i64) -> ParameterMode {
        if i == 0 {
            ParameterMode::Position
        }
        else if i == 1 {
            ParameterMode::Immediate
        }
        else if i == 2 {
            ParameterMode::Relative
        }
        else {
            panic!("Unknown parameter mode integer value: {}", i);
        }
    }
}

impl Computer {
    pub fn initialize(program: Vec<i64>, input: Vec<i64>) -> Self {
        let mut computer = Computer {
            memory: HashMap::new(),
            position: 0,
            relative_base: 0,
            input: VecDeque::from(input),
            output: Vec::new(),
        };
        for i in 0..program.len() {
            computer.mem_put(i, program[i]);
        }
        return computer;
    }

    fn mem_put(&mut self, address: usize, value: i64) {
        self.memory.insert(address, value);
    }

    fn mem_get(&mut self, address: usize) -> i64 {
        let value = self.memory.get(&address);
        if value.is_none() {
            self.mem_put(address, 0);
            return 0;
        }
        return *value.unwrap();
    }

    pub fn run(&mut self) -> Status {
        loop {
            let (op, p1_mode, p2_mode, p3_mode) = Self::decode_instruction(
                self.mem_get(self.position)
            );
            match op {
                Operation::Add => {
                    let d1 = self.fetch(self.position + 1, p1_mode);
                    let d2 = self.fetch(self.position + 2, p2_mode);
                    let dest = self.absolute_address(self.position + 3, p3_mode);
                    self.mem_put(dest, d1 + d2);
                    self.position += 4;
                },
                Operation::Mul => {
                    let d1 = self.fetch(self.position + 1, p1_mode);
                    let d2 = self.fetch(self.position + 2, p2_mode);
                    let dest = self.absolute_address(self.position + 3, p3_mode);
                    self.mem_put(dest, d1 * d2);
                    self.position += 4;
                },
                Operation::Input => {
                    if self.input.len() == 0 {
                        return Status::WaitingForInput;
                    }
                    let dest = self.absolute_address(self.position + 1, p1_mode);
                    let input = self.input.pop_front().expect("Input queue empty... weird");
                    self.mem_put(dest, input);
                    println!("Input: {}", input);
                    self.position += 2;
                },
                Operation::Output => {
                    let value = self.fetch(self.position + 1, p1_mode);
                    println!("Output: {}", value);
                    self.output.push(value);
                    self.position += 2;
                },
                Operation::JumpIfTrue => {
                    let d1 = self.fetch(self.position + 1, p1_mode);
                    let d2 = self.fetch(self.position + 2, p2_mode);
                    if d1 != 0 {
                        self.position = d2 as usize;
                    }
                    else {
                        self.position += 3;
                    }
                },
                Operation::JumpIfFalse => {
                    let d1 = self.fetch(self.position + 1, p1_mode);
                    let d2 = self.fetch(self.position + 2, p2_mode);
                    if d1 == 0 {
                        self.position = d2 as usize;
                    }
                    else {
                        self.position += 3;
                    }
                },
                Operation::LessThan => {
                    let d1 = self.fetch(self.position + 1, p1_mode);
                    let d2 = self.fetch(self.position + 2, p2_mode);
                    let dest = self.absolute_address(self.position + 3, p3_mode);
                    self.mem_put(dest, if d1 < d2 { 1 } else { 0 });
                    self.position += 4;
                },
                Operation::Equals => {
                    let d1 = self.fetch(self.position + 1, p1_mode);
                    let d2 = self.fetch(self.position + 2, p2_mode);
                    let dest = self.absolute_address(self.position + 3, p3_mode);
                    self.mem_put(dest, if d1 == d2 { 1 } else { 0 });
                    self.position += 4;
                },
                Operation::AdjustRelativeBase => {
                    self.relative_base += self.fetch(self.position + 1, p1_mode);
                    //println!("[RB: {}]", self.relative_base);
                    self.position += 2;
                },
                Operation::End => {
                    break;
                },
                other => {
                    panic!("'{:?}' not implemented", other);
                },
            };
        }
        return Status::Finished;
    }

    fn fetch(&mut self, pos: usize, mode: ParameterMode) -> i64 {
        let addr = self.absolute_address(pos, mode);
        return self.mem_get(addr);
    }

    fn absolute_address(&mut self, pos: usize, mode: ParameterMode) -> usize {
        match mode {
            ParameterMode::Position => {
                self.mem_get(
                    pos
                ) as usize
            },
            ParameterMode::Immediate => {
                pos
            },
            ParameterMode::Relative => {
                let p = self.mem_get(pos) as i64 + self.relative_base;
                if p < 0 {
                    panic!("Attempting to resolve illegale address: {}", p);
                }
                return p as usize;
            }
        }
    }

    fn decode_instruction(code: i64) -> (Operation, ParameterMode, ParameterMode, ParameterMode) {
        // Opcodes are ABCDE, where the op is DE
        // the modes for params 1, 2, and 3 as C, B, A
        // respectively.
        let mut c = code;
        let op_int = code % 100;
        let op = if op_int == 1 {
            Operation::Add
        } else if op_int == 2 {
            Operation::Mul
        } else if op_int == 3 {
            Operation::Input
        } else if op_int == 4 {
            Operation::Output
        } else if op_int == 5 {
            Operation::JumpIfTrue
        } else if op_int == 6 {
            Operation::JumpIfFalse
        } else if op_int == 7 {
            Operation::LessThan
        } else if op_int == 8 {
            Operation::Equals
        } else if op_int == 9 {
            Operation::AdjustRelativeBase
        }else if op_int == 99 {
            Operation::End
        } else {
            panic!("Unknown opcode: {}", op_int);
        };

        let param_3_mode_int = c / 10000;
        c -= param_3_mode_int * 10000;
        let param_2_mode_int = c / 1000;
        c -= param_2_mode_int * 1000;
        let param_1_mode_int = c / 100;
        return (
            op,
            ParameterMode::from_int(param_1_mode_int),
            ParameterMode::from_int(param_2_mode_int),
            ParameterMode::from_int(param_3_mode_int),
        );
    }
}

pub fn simulate(v: &mut Vec<i32>, input: &mut VecDeque<i32>, output: &mut Vec<i32>, start: usize) -> Result {
    let mut program: Vec<i64> = Vec::new();
    for i in 0..v.len() {
        program.push(i64::from(v[i]));
    }
    let mut ip: VecDeque<i64> = VecDeque::new();
    for i in 0..input.len() {
        ip.push_front(i64::from(input[i]));
    }
    let mut computer = Computer::initialize(program, vec![]);
    computer.input = ip;
    computer.position = start;
    let r = computer.run();
    for i in 0..computer.output.len() {
        if computer.output[i] <= i64::from(i32::MAX) && computer.output[i] >= i64::from(i32::MIN) {
            output.push(computer.output[i] as i32);
        }
        else {
            panic!("Output value '{}' out of range for legacy i32 int-code computer", computer.output[i]);
        }
    }
    for i in 0..v.len() {
        v[i] = computer.mem_get(i) as i32;
    }
    return Result {
        status: r,
        instruction: computer.position,
    };
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_op_1() {
        let mut program = vec![1, 0, 0, 0, 99];
        let mut input: VecDeque<i32> = VecDeque::new();
        let mut output: Vec<i32> = Vec::new();
        simulate(&mut program, &mut input, &mut output, 0);
        assert_eq!(program[0], 2);
    }

    #[test]
    fn test_op_2() {
        let mut program = vec![2, 3, 0, 3, 99];
        let mut input: VecDeque<i32> = VecDeque::new();
        let mut output: Vec<i32> = Vec::new();
        simulate(&mut program, &mut input, &mut output, 0);
        assert_eq!(program[3], 6);
    }

    #[test]
    fn test_case_3() {
        let mut program = vec![2,4,4,5,99,0];
        let mut input: VecDeque<i32> = VecDeque::new();
        let mut output: Vec<i32> = Vec::new();
        simulate(&mut program, &mut input, &mut output, 0);
        assert_eq!(program[program.len() - 1], 9801);
    }

    #[test]
    fn test_case_4() {
        let mut program = vec![1,1,1,4,99,5,6,0,99];
        let mut input: VecDeque<i32> = VecDeque::new();
        let mut output: Vec<i32> = Vec::new();
        simulate(&mut program, &mut input, &mut output, 0);
        assert_eq!(program[0], 30);
        assert_eq!(program[4], 2);
    }

    #[test]
    fn immediate_mode_multiply() {
        let mut program = vec![1002,4,3,4,33];
        let mut input: VecDeque<i32> = VecDeque::new();
        let mut output: Vec<i32> = Vec::new();
        simulate(&mut program, &mut input, &mut output, 0);
        assert_eq!(program[4], 99);
    }

    #[test]
    fn negative_integers() {
        let mut program = vec![1101,100,-1,4,0];
        let mut input: VecDeque<i32> = VecDeque::new();
        let mut output: Vec<i32> = Vec::new();
        simulate(&mut program, &mut input, &mut output, 0);
        assert_eq!(program[4], 99);
    }

    #[test]
    fn position_mode_input_equal_to_8() {
        let mut program = vec![3,9,8,9,10,9,4,9,99,-1,8];
        let mut output: Vec<i32> = Vec::new();
        let backup = program.clone();
        simulate(&mut program, &mut VecDeque::from([8]), &mut output, 0);
        assert_eq!(output[0], 1);
        output.clear();
        program = backup.clone();
        simulate(&mut program, &mut VecDeque::from([7]), &mut output, 0);
        assert_eq!(output[0], 0);
    }

    #[test]
    fn position_mode_input_less_than_8() {
        let mut program = vec![3,9,7,9,10,9,4,9,99,-1,8];
        let mut output: Vec<i32> = Vec::new();
        let backup = program.clone();
        simulate(&mut program, &mut VecDeque::from([8]), &mut output, 0);
        assert_eq!(output[0], 0);
        output.clear();
        program = backup.clone();
        simulate(&mut program, &mut VecDeque::from([7]), &mut output, 0);
        assert_eq!(output[0], 1);
    }

    #[test]
    fn immediate_mode_input_equal_to_8() {
        let mut program = vec![3,3,1108,-1,8,3,4,3,99];
        let mut output: Vec<i32> = Vec::new();
        let backup = program.clone();
        simulate(&mut program, &mut VecDeque::from([8]), &mut output, 0);
        assert_eq!(output[0], 1);
        output.clear();
        program = backup.clone();
        simulate(&mut program, &mut VecDeque::from([7]), &mut output, 0);
        assert_eq!(output[0], 0);
    }

    #[test]
    fn immediate_mode_input_less_than_8() {
        let mut program = vec![3,3,1107,-1,8,3,4,3,99];
        let mut output: Vec<i32> = Vec::new();
        let backup = program.clone();
        simulate(&mut program, &mut VecDeque::from([8]), &mut output, 0);
        assert_eq!(output[0], 0);
        output.clear();
        program = backup.clone();
        simulate(&mut program, &mut VecDeque::from([7]), &mut output, 0);
        assert_eq!(output[0], 1);
    }

    #[test]
    fn position_mode_input_is_non_zero() {
        let mut program = vec![3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9];
        let mut output: Vec<i32> = Vec::new();
        let backup = program.clone();
        simulate(&mut program, &mut VecDeque::from([8]), &mut output, 0);
        assert_eq!(output[0], 1);
        output.clear();
        program = backup.clone();
        simulate(&mut program, &mut VecDeque::from([0]), &mut output, 0);
        assert_eq!(output[0], 0);
    }

    #[test]
    fn immediate_mode_input_is_non_zero() {
        let mut program = vec![3,3,1105,-1,9,1101,0,0,12,4,12,99,1];
        let mut output: Vec<i32> = Vec::new();
        let backup = program.clone();
        simulate(&mut program, &mut VecDeque::from([9]), &mut output, 0);
        assert_eq!(output[0], 1);
        output.clear();
        program = backup.clone();
        simulate(&mut program, &mut VecDeque::from([0]), &mut output, 0);
        assert_eq!(output[0], 0);
    }

    #[test]
    fn multi_jump() {
        let mut program = vec![3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99];
        let mut output: Vec<i32> = Vec::new();
        let backup = program.clone();
        simulate(&mut program, &mut VecDeque::from([7]), &mut output, 0);
        assert_eq!(output[0], 999);
        output.clear();
        program = backup.clone();
        simulate(&mut program, &mut VecDeque::from([8]), &mut output, 0);
        assert_eq!(output[0], 1000);
        output.clear();
        program = backup.clone();
        simulate(&mut program, &mut VecDeque::from([9]), &mut output, 0);
        assert_eq!(output[0], 1001);
    }

    #[test]
    fn blocks_on_input() {
        let mut program = vec![3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5];
        let mut output: Vec<i32> = Vec::new();
        let result = simulate(&mut program, &mut VecDeque::from([9, 0]), &mut output, 0);
        assert_eq!(result.status, Status::WaitingForInput);
        assert_eq!(result.instruction, 6);
    }

    #[test]
    fn relative_base_adjust() {
        let mut c = Computer::initialize(vec![109, 1, 99], vec![]);
        c.run();
        assert_eq!(c.relative_base, 1);
    }

    #[test]
    #[should_panic]
    fn negative_memory_access() {
        let mut c = Computer::initialize(vec![109, -5000, 204, 0, 99], vec![]);
        c.run();
    }

    #[test]
    fn relative_example_1() {
        // The program produces an output of itself
        let program = vec![109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99];
        let mut c = Computer::initialize(program.clone(), vec![]);
        c.run();
        for i in 0..program.len() {
            assert_eq!(program[i], c.output[i]);
        }
    }

    #[test]
    fn relative_example_2() {
        // Outputs a 16 digit numbers
        let program = vec![1102,34915192,34915192,7,4,7,99,0];
        let mut c = Computer::initialize(program, vec![]);
        c.run();
        assert_eq!(c.output[0].to_string().len(), 16);
    }

    #[test]
    fn relative_example_3() {
        // Outputs 1125899906842624
        let program = vec![104,1125899906842624,99];
        let mut c = Computer::initialize(program, vec![]);
        c.run();
        assert_eq!(c.output[0], 1125899906842624);
    }

    #[test]
    fn day9_relative_mode_input() {
        let program = vec![109, -2, 203, 2, 99];
        let mut c = Computer::initialize(program, vec![1312]);
        c.run();
        assert_eq!(c.mem_get(0), 1312);
    }

    #[test]
    fn day9() {
        let program = vec![1102,34463338,34463338,63,1007,63,34463338,63,1005,63,53,1101,0,3,1000,109,988,209,12,9,1000,209,6,209,3,203,0,1008,1000,1,63,1005,63,65,1008,1000,2,63,1005,63,904,1008,1000,0,63,1005,63,58,4,25,104,0,99,4,0,104,0,99,4,17,104,0,99,0,0,1102,1,432,1027,1101,439,0,1026,1101,0,36,1010,1101,0,34,1018,1102,278,1,1029,1101,0,24,1002,1102,1,20,1016,1102,1,31,1011,1102,319,1,1024,1102,21,1,1012,1102,1,763,1022,1102,1,25,1007,1101,0,287,1028,1102,32,1,1008,1101,0,22,1013,1102,38,1,1001,1101,0,314,1025,1102,35,1,1009,1102,1,23,1015,1102,39,1,1019,1102,27,1,1000,1102,1,37,1003,1102,1,28,1017,1101,0,0,1020,1101,0,29,1004,1102,1,30,1006,1102,1,756,1023,1102,1,33,1005,1101,0,1,1021,1102,26,1,1014,109,13,2108,28,-7,63,1005,63,201,1001,64,1,64,1105,1,203,4,187,1002,64,2,64,109,8,21107,40,41,-3,1005,1018,225,4,209,1001,64,1,64,1105,1,225,1002,64,2,64,109,-3,1206,2,239,4,231,1105,1,243,1001,64,1,64,1002,64,2,64,109,-21,1201,6,0,63,1008,63,35,63,1005,63,267,1001,64,1,64,1105,1,269,4,249,1002,64,2,64,109,35,2106,0,-4,4,275,1001,64,1,64,1105,1,287,1002,64,2,64,109,-11,1205,-1,303,1001,64,1,64,1105,1,305,4,293,1002,64,2,64,109,8,2105,1,-5,4,311,1106,0,323,1001,64,1,64,1002,64,2,64,109,-7,21108,41,38,-6,1005,1016,339,1106,0,345,4,329,1001,64,1,64,1002,64,2,64,109,2,21102,42,1,-8,1008,1016,45,63,1005,63,369,1001,64,1,64,1105,1,371,4,351,1002,64,2,64,109,-14,21101,43,0,1,1008,1011,43,63,1005,63,397,4,377,1001,64,1,64,1106,0,397,1002,64,2,64,109,-8,21101,44,0,8,1008,1010,47,63,1005,63,417,1105,1,423,4,403,1001,64,1,64,1002,64,2,64,109,25,2106,0,0,1001,64,1,64,1105,1,441,4,429,1002,64,2,64,109,-20,2107,37,-6,63,1005,63,463,4,447,1001,64,1,64,1106,0,463,1002,64,2,64,109,8,2108,25,-8,63,1005,63,485,4,469,1001,64,1,64,1106,0,485,1002,64,2,64,109,-1,21107,45,44,-1,1005,1013,505,1001,64,1,64,1106,0,507,4,491,1002,64,2,64,109,-11,1207,-1,25,63,1005,63,529,4,513,1001,64,1,64,1106,0,529,1002,64,2,64,109,23,1206,-5,545,1001,64,1,64,1106,0,547,4,535,1002,64,2,64,109,-31,2102,1,5,63,1008,63,27,63,1005,63,569,4,553,1106,0,573,1001,64,1,64,1002,64,2,64,109,27,21102,46,1,-9,1008,1013,46,63,1005,63,595,4,579,1105,1,599,1001,64,1,64,1002,64,2,64,109,-26,2101,0,6,63,1008,63,24,63,1005,63,625,4,605,1001,64,1,64,1106,0,625,1002,64,2,64,109,5,1208,0,37,63,1005,63,645,1001,64,1,64,1105,1,647,4,631,1002,64,2,64,109,7,2102,1,-3,63,1008,63,31,63,1005,63,671,1001,64,1,64,1105,1,673,4,653,1002,64,2,64,109,2,1202,-5,1,63,1008,63,33,63,1005,63,699,4,679,1001,64,1,64,1105,1,699,1002,64,2,64,109,-4,2101,0,-3,63,1008,63,35,63,1005,63,719,1105,1,725,4,705,1001,64,1,64,1002,64,2,64,109,-5,1207,4,32,63,1005,63,741,1106,0,747,4,731,1001,64,1,64,1002,64,2,64,109,29,2105,1,-7,1001,64,1,64,1106,0,765,4,753,1002,64,2,64,109,-26,2107,36,5,63,1005,63,781,1105,1,787,4,771,1001,64,1,64,1002,64,2,64,109,10,1201,-6,0,63,1008,63,32,63,1005,63,809,4,793,1106,0,813,1001,64,1,64,1002,64,2,64,109,3,21108,47,47,-5,1005,1012,835,4,819,1001,64,1,64,1106,0,835,1002,64,2,64,109,-24,1202,9,1,63,1008,63,25,63,1005,63,859,1001,64,1,64,1106,0,861,4,841,1002,64,2,64,109,19,1205,9,875,4,867,1106,0,879,1001,64,1,64,1002,64,2,64,109,-3,1208,-1,32,63,1005,63,897,4,885,1106,0,901,1001,64,1,64,4,64,99,21102,27,1,1,21101,915,0,0,1105,1,922,21201,1,60043,1,204,1,99,109,3,1207,-2,3,63,1005,63,964,21201,-2,-1,1,21102,1,942,0,1106,0,922,21202,1,1,-1,21201,-2,-3,1,21101,957,0,0,1106,0,922,22201,1,-1,-2,1105,1,968,22102,1,-2,-2,109,-3,2105,1,0];
        let mut c = Computer::initialize(program, vec![1]);
        c.run();
        assert_eq!(c.output.len(), 1);
        assert_eq!(c.output[0], 3063082071);
    }
}
